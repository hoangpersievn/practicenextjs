import { ApolloClient } from 'apollo-client';
import { ApolloLink, split } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { onError } from 'apollo-link-error';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createUploadLink } from 'apollo-upload-client';
import { getMainDefinition } from 'apollo-utilities';
import fetch from 'isomorphic-fetch';

const API_HOST_NAME = 'staging-api.data-advising.net';

const config = {
  GRAPHQL_HTTP_URI: `https://${API_HOST_NAME}/graphql`,
  GRAPHQL_WS_URI: `wss://${API_HOST_NAME}/graphql`
}

if (!process.browser) {
  global.fetch = fetch;
}

const httpLink = createUploadLink({
  uri: config.GRAPHQL_HTTP_URI,
  credentials: 'include',
  fetchOptions: {
    // mode: 'no-cors'
    'Access-Control-Allow-Credentials': true
  }
});

// debugger

const wsLink = process.browser
  ? new WebSocketLink(
      new SubscriptionClient(config.GRAPHQL_WS_URI, {
        reconnect: true
      })
    )
  : null;

const terminatingLink = process.browser
  ? split(
      ({ query }) => {
        const { kind, operation } = getMainDefinition(query);
        return kind === 'OperationDefinition' && operation === 'subscription';
      },
      wsLink,
      httpLink
    )
  : httpLink;

const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'cache-and-network',
    errorPolicy: 'ignore'
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all'
  },
  mutate: {
    errorPolicy: 'all'
  }
};

const client = new ApolloClient({
  connectToDevTools: process.browser,
  ssrMode: !process.browser,
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
          // eslint-disable-next-line
          console.error(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
          )
        );
      // eslint-disable-next-line
      if (networkError) console.error(`[Network error]: ${networkError}`);
    }),
    terminatingLink
  ]),
  cache: new InMemoryCache(),
  defaultOptions
  // defaultOptions: {
  //   watchQuery: {
  //     fetchPolicy: 'cache-and-network'
  //   },
  //   query: {
  //     fetchPolicy: 'no-cache'
  //   }
  // }
});

export default client;
