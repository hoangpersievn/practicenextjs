const createRequestContibute = (base, act) => {

  return ['REQUEST', 'SUCCESS', 'FAILURE'].reduce((acc, type) => {
    const key = `${act}_${type}`;
    acc[key] = `${base}_${act}_${type}`;
    return acc;
  }, {});
};

export default {
  ...createRequestContibute('CONTRIBUTE', 'FETCH_CONTRIBUTORS'),
  ...createRequestContibute('CONTRIBUTE', 'CREATE_CONTRIBUTOR')
};