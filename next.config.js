const withImages = require('next-images');
const withCSS = require('@zeit/next-css');
const withSASS = require('@zeit/next-sass');

module.exports = withImages(
  withCSS(
    withSASS()
  )
)