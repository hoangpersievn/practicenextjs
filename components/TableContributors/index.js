import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Table } from 'antd';
import uuid from 'uuid/v1';

import { actFetchContributors } from '../../actions/contribute';
import { isEmpty } from '../../utils/func';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name'
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email'
  },
  {
    title: 'Url',
    dataIndex: 'url',
    key: 'url'
  },
  {
    title: 'Note',
    dataIndex: 'note',
    key: 'note'
  }
];

const TableContributors = (props) => {
  const [data, setData] = useState([]);
  const { actFetchContributors, contributors } = props;

  useEffect(() => {
    if(isEmpty(contributors))
      actFetchContributors();
  }, []);

  useEffect(() => {
    const result = contributors.map( item  => {
      return {
        ...item,
        key: uuid()
      }
    })
    setData(result);  
  }, [contributors]);

  return (
    <div className="TableContributors">
      <Table columns={columns} dataSource={data}/>
    </div>
  );
};

const mapStateToProps = state => {
  const { contributors } = state.contribute;
  
  return {
    contributors
  }
}

TableContributors.propTypes = {
  contributors: PropTypes.array,
  actFetchContributors: PropTypes.func
}

export default connect(mapStateToProps, {
  actFetchContributors
})(TableContributors);