import React from 'react';
import { Calendar } from 'antd';
import { connect } from 'react-redux';

import { submitContribute } from '../../actions/contribute';

import FormContribute from './FormContribute';

const Contribute = (props) => {

  const onPanelChange = (value, mode) => {
    console.log(value, mode);
  }

  return (
    <div className="Contribute">
      <div className="Contribute__calendar">
        <Calendar onPanelChange={() => onPanelChange} />
      </div>
      <FormContribute {...props}/>
    </div>
  )
};

export default connect(null, {
  submitContribute
})(Contribute);