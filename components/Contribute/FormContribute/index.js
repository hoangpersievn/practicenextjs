import React from 'react';
import {
  Form,
  Input,
  Button,
} from 'antd';

const { TextArea } = Input;
const FormContribute = (props) => {
  const { getFieldDecorator, validateFields, resetFields } = props.form;
  const { submitContribute } = props;

  const handleSubmit = (e) => {
    e.preventDefault();
    validateFields((err, values) => {
      if(!err){
        submitContribute(values)
        resetFields();
      }
    });
  }
  return (
    <div className="FormContribute">
      <Form onSubmit={handleSubmit}>
        <Form.Item label="User name">
          {getFieldDecorator("name", {
            rules: [
              {
                required: true,
                message: "Please input your name!"
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="E-mail">
          {getFieldDecorator("email", {
            rules: [
              {
                type: "email",
                message: "The input is not valid E-mail!"
              },
              {
                required: true,
                message: "Please input your E-mail!"
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Url">
          {getFieldDecorator("url", {
            rules: [
              {
                required: true,
                message: "Please input your url!"
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Note">
          {getFieldDecorator("note", {
          })(<TextArea rows={4}/>)}
        </Form.Item>
        <Button type="primary" htmlType="submit" block>
          SEND TO US
        </Button>
      </Form>
    </div>
  );
};

export default Form.create({ name: 'contribute'})(FormContribute);