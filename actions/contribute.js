import gql from 'graphql-tag';

import { CONTRIBUTE_TYPE } from '../constTypes';
import client from '../core/apollo';

export const actFetchContributors = () => dispatch => {
  const queryParams = {
    query: gql`
      query getContributors {
        contributors {
          name
          email
          url
          note
        }
      }
    `,
    operationName: 'getContributors',
    valiables: {}
  };

  dispatch({
    type: CONTRIBUTE_TYPE.FETCH_CONTRIBUTORS_REQUEST
  });
  return client
    .query(queryParams)
    .then(res => {
      const { contributors } = res.data;
      dispatch({
        type: CONTRIBUTE_TYPE.FETCH_CONTRIBUTORS_SUCCESS,
        payload: contributors
      })
    })
    .catch( err => {
      dispatch({
        type: CONTRIBUTE_TYPE.FETCH_CONTRIBUTORS_FAILURE,
        payload: err
      })
    });
};

export const submitContribute = dataSubmit => dispatch => {
  const mutateParams = {
    mutation: gql`
      mutation($email: String!, $name: String, $url: String!, $note: String) {
        create_contributor(email: $email, name: $name, url: $url, note: $note) {
          contributor {
            id
            name
          }
        }
      }
    `,
    operationName: "createContributor",
    valiables: { ...dataSubmit }
  };

  dispatch({
    type: CONTRIBUTE_TYPE.CREATE_CONTRIBUTOR_REQUEST
  });
  return client
    .mutate(mutateParams)
    .then(res => {
      const { create_contributor } = res.data;
      
      dispatch({
        type: CONTRIBUTE_TYPE.CREATE_SUCCESS,
        payload: create_contributor
      })
    })
    .catch(err => {
      dispatch({
        type: CONTRIBUTE_TYPE.CREATE_CONTRIBUTOR_FAILURE,
        payload: err
      })
    })
};