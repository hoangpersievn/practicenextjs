import { CONTRIBUTE_TYPE } from '../constTypes';

const initialState = {
  contributors: [],
  contributor: null,
  error: null,
  contributorError: null
};

export default (state=initialState, action) => {

  switch(action.type) {

    case CONTRIBUTE_TYPE.FETCH_CONTRIBUTORS_SUCCESS:
      return {
        ...state,
        contributors: action.payload,
        error: null
      };
    case CONTRIBUTE_TYPE.FETCH_CONTRIBUTORS_FAILURE:
      return {
        ...state,
        error: action.payload
      };
    case CONTRIBUTE_TYPE.CREATE_CONTRIBUTOR_SUCCESS:
      console.log(action.payload)
      return {
        ...state,
        contributor: action.payload,
        contributorError: null
      };
    case CONTRIBUTE_TYPE.CREATE_CONTRIBUTOR_FAILURE:
      console.log(action.payload)
      return {
        ...state,
        contributorError: action.payload
      }
    default:
      return state;
  }
};