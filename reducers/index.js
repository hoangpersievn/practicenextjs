import { combineReducers } from 'redux';

import contribute from './contribute';

export default combineReducers({
  contribute
});