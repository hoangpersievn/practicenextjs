webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/TableContributors/index.js":
/*!***********************************************!*\
  !*** ./components/TableContributors/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _actions_contribute__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../actions/contribute */ "./actions/contribute.js");
var _jsxFileName = "/Users/hoangtruong/Documents/Code_dao/Rac/damsan/contribute4/components/TableContributors/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




var columns = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name'
}, {
  title: 'Email',
  dataIndex: 'email',
  key: 'email'
}, {
  title: 'Url',
  dataIndex: 'url',
  key: 'url'
}, {
  title: 'Note',
  dataIndex: 'note',
  key: 'note'
}];

var TableContributors = function TableContributors(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      data = _useState[0],
      setData = _useState[1];

  var actFetchContributors = props.actFetchContributors,
      contributors = props.contributors;
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Table"], {
    columns: columns,
    dataSource: data,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }));
};

var mapStateToProps = function mapStateToProps(state) {
  var contributors = state.contribute.contributors;
  return {
    contributors: contributors
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, {
  actFetchContributors: _actions_contribute__WEBPACK_IMPORTED_MODULE_3__["actFetchContributors"]
})(TableContributors));

/***/ })

})
//# sourceMappingURL=index.js.a4efa81940ac380645a2.hot-update.js.map