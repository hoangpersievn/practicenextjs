webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/Contribute/index.js":
/*!****************************************!*\
  !*** ./components/Contribute/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _actions_contribute__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../actions/contribute */ "./actions/contribute.js");
/* harmony import */ var _FormContribute__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./FormContribute */ "./components/Contribute/FormContribute/index.js");

var _jsxFileName = "/Users/hoangtruong/Documents/Code_dao/Rac/damsan/contribute4/components/Contribute/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;






var Contribute = function Contribute(props) {
  var _onPanelChange = function onPanelChange(value, mode) {
    console.log(value, mode);
  };

  return __jsx("div", {
    className: "Contribute",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, __jsx("div", {
    className: "Contribute__calendar",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Calendar"], {
    onPanelChange: function onPanelChange() {
      return _onPanelChange;
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  })), __jsx(_FormContribute__WEBPACK_IMPORTED_MODULE_5__["default"], Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, props, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["connect"])(null, {
  submitContribute: _actions_contribute__WEBPACK_IMPORTED_MODULE_4__["submitContribute"]
})(Contribute));

/***/ })

})
//# sourceMappingURL=index.js.6f998f04fe7e0422be71.hot-update.js.map