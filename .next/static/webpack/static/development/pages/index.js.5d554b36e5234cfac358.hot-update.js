webpackHotUpdate("static/development/pages/index.js",{

/***/ "./actions/contribute.js":
/*!*******************************!*\
  !*** ./actions/contribute.js ***!
  \*******************************/
/*! exports provided: actFetchContributors, submitContribute */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "actFetchContributors", function() { return actFetchContributors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "submitContribute", function() { return submitContribute; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constTypes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../constTypes */ "./constTypes/index.js");
/* harmony import */ var _core_apollo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/apollo */ "./core/apollo.js");


function _templateObject2() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])([""]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n      query getContributors {\n        contributors {\n          name\n          email\n          url\n          note\n        }\n      }\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}




var actFetchContributors = function actFetchContributors() {
  return function (dispatch) {
    var queryParams = {
      query: graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(_templateObject()),
      operationName: 'getContributors',
      valiables: {}
    };
    dispatch({
      type: _constTypes__WEBPACK_IMPORTED_MODULE_2__["CONTRIBUTE_TYPE"].FETCH_CONTRIBUTORS_REQUEST
    });
    return _core_apollo__WEBPACK_IMPORTED_MODULE_3__["default"].query(queryParams).then(function (res) {
      var contributors = res.data.contributors;
      dispatch({
        type: _constTypes__WEBPACK_IMPORTED_MODULE_2__["CONTRIBUTE_TYPE"].FETCH_CONTRIBUTORS_SUCCESS,
        payload: contributors
      });
    })["catch"](function (err) {
      dispatch({
        type: _constTypes__WEBPACK_IMPORTED_MODULE_2__["CONTRIBUTE_TYPE"].FETCH_CONTRIBUTORS_FAILURE,
        payload: err
      });
    });
  };
};
var submitContribute = function submitContribute(dataSubmit) {
  return function (dispatch) {
    var mutateParams = {
      mutation: graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(_templateObject2()),
      operationName: 'createContributor',
      valiables: dataSubmit
    };
  };
};

/***/ })

})
//# sourceMappingURL=index.js.5d554b36e5234cfac358.hot-update.js.map