webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/Contribute/FormContribute/index.js":
/*!*******************************************************!*\
  !*** ./components/Contribute/FormContribute/index.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
var _jsxFileName = "/Users/hoangtruong/Documents/Code_dao/Rac/damsan/contribute4/components/Contribute/FormContribute/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


var TextArea = antd__WEBPACK_IMPORTED_MODULE_1__["Input"].TextArea;

var FormContribute = function FormContribute(props) {
  var getFieldDecorator = props.form.getFieldDecorator;
  return __jsx("div", {
    className: "FormContribute",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"].Item, {
    label: "User name",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, getFieldDecorator("name", {
    rules: [{
      required: true,
      message: "Please input your name!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"].Item, {
    label: "E-mail",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, getFieldDecorator("email", {
    rules: [{
      type: "email",
      message: "The input is not valid E-mail!"
    }, {
      required: true,
      message: "Please input your E-mail!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"].Item, {
    label: "Url",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }, getFieldDecorator("url", {
    rules: [{
      required: true,
      message: "Please input your url!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"].item, {
    label: "Note",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }, (getFieldDecorator("note"), {}(__jsx(TextArea, {
    rows: 4,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  }))))));
};

/* harmony default export */ __webpack_exports__["default"] = (antd__WEBPACK_IMPORTED_MODULE_1__["Form"].create({
  name: 'contribute'
})(FormContribute));

/***/ })

})
//# sourceMappingURL=index.js.d87f0a604c091c01cda8.hot-update.js.map