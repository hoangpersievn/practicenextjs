webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/Contribute/FormContribute/index.js":
/*!*******************************************************!*\
  !*** ./components/Contribute/FormContribute/index.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
var _jsxFileName = "/Users/hoangtruong/Documents/Code_dao/Rac/damsan/contribute4/components/Contribute/FormContribute/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


var TextArea = antd__WEBPACK_IMPORTED_MODULE_1__["Input"].TextArea;

var FormContribute = function FormContribute(props) {
  var _props$form = props.form,
      getFieldDecorator = _props$form.getFieldDecorator,
      validateFields = _props$form.validateFields,
      resetFields = _props$form.resetFields;
  var submitContribute = props.submitContribute;

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    validateFields(function (err, values) {
      if (!err) {
        submitContribute(values);
        resetFields();
      }
    });
  };

  return __jsx("div", {
    className: "FormContribute",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"], {
    onSubmit: function onSubmit(e) {
      return handleSubmit(e);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"].Item, {
    label: "User name",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, getFieldDecorator("name", {
    rules: [{
      required: true,
      message: "Please input your name!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"].Item, {
    label: "E-mail",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, getFieldDecorator("email", {
    rules: [{
      type: "email",
      message: "The input is not valid E-mail!"
    }, {
      required: true,
      message: "Please input your E-mail!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"].Item, {
    label: "Url",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }, getFieldDecorator("url", {
    rules: [{
      required: true,
      message: "Please input your url!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Form"].Item, {
    label: "Note",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  }, getFieldDecorator("note", {})(__jsx(TextArea, {
    rows: 4,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    type: "primary",
    block: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63
    },
    __self: this
  }, "SEND TO US")));
};

/* harmony default export */ __webpack_exports__["default"] = (antd__WEBPACK_IMPORTED_MODULE_1__["Form"].create({
  name: 'contribute'
})(FormContribute));

/***/ })

})
//# sourceMappingURL=index.js.7afaf1a4861099611f9b.hot-update.js.map