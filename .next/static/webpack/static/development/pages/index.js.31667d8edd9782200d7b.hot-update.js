webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/TableContributors/index.js":
/*!***********************************************!*\
  !*** ./components/TableContributors/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _actions_contribute__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../actions/contribute */ "./actions/contribute.js");
/* harmony import */ var _utils_func__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../utils/func */ "./utils/func.js");
var _jsxFileName = "/Users/hoangtruong/Documents/Code_dao/Rac/damsan/contribute4/components/TableContributors/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





var columns = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name'
}, {
  title: 'Email',
  dataIndex: 'email',
  key: 'email'
}, {
  title: 'Url',
  dataIndex: 'url',
  key: 'url'
}, {
  title: 'Note',
  dataIndex: 'note',
  key: 'note'
}];

var TableContributors = function TableContributors(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      data = _useState[0],
      setData = _useState[1];

  var actFetchContributors = props.actFetchContributors,
      contributors = props.contributors;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    if (Object(_utils_func__WEBPACK_IMPORTED_MODULE_4__["isEmpty"])(contributors)) actFetchContributors();
  }, []);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    setData(contributors);
  }, [contributors]);
  return __jsx("div", {
    className: "TableContributors",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Table"], {
    columns: columns,
    dataSource: data,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }));
};

var mapStateToProps = function mapStateToProps(state) {
  var contributors = state.contribute.contributors;
  return {
    contributors: contributors
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, {
  actFetchContributors: _actions_contribute__WEBPACK_IMPORTED_MODULE_3__["actFetchContributors"]
})(TableContributors));

/***/ }),

/***/ "./node_modules/uuid/lib/bytesToUuid.js":
false,

/***/ "./node_modules/uuid/lib/rng-browser.js":
false,

/***/ "./node_modules/uuid/v1.js":
false

})
//# sourceMappingURL=index.js.31667d8edd9782200d7b.hot-update.js.map