webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/Contribute/FormContribute/index.js":
/*!*******************************************************!*\
  !*** ./components/Contribute/FormContribute/index.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _actions_contribute__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../actions/contribute */ "./actions/contribute.js");
var _jsxFileName = "/Users/hoangtruong/Documents/Code_dao/Rac/damsan/contribute4/components/Contribute/FormContribute/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




var TextArea = antd__WEBPACK_IMPORTED_MODULE_2__["Input"].TextArea;

var FormContribute = function FormContribute(props) {
  var getFieldDecorator = props.form.getFieldDecorator;
  return __jsx("div", {
    className: "FormContribute",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Form"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
    label: "User name",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, getFieldDecorator("name", {
    rules: [{
      required: true,
      message: "Please input your name!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
    label: "E-mail",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, getFieldDecorator("email", {
    rules: [{
      type: "email",
      message: "The input is not valid E-mail!"
    }, {
      required: true,
      message: "Please input your E-mail!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
    label: "Url",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, getFieldDecorator("url", {
    rules: [{
      required: true,
      message: "Please input your url!"
    }]
  })(__jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Input"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Form"].Item, {
    label: "Note",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }, getFieldDecorator("note", {})(__jsx(TextArea, {
    rows: 4,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }))), __jsx(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    type: "primary",
    block: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, "SEND TO US")));
};

/* harmony default export */ __webpack_exports__["default"] = (antd__WEBPACK_IMPORTED_MODULE_2__["Form"].create({
  name: 'contribute'
})(FormContribute));

/***/ })

})
//# sourceMappingURL=index.js.600bbde57a20818b2f29.hot-update.js.map