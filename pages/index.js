import React, { Component } from 'react';

import TableContributors from '../components/TableContributors';
import Contribute from '../components/Contribute';

class HomePage extends Component {

  render () {

    return (
      <div className="App">
        <Contribute/>
        <TableContributors/>
      </div>
    );
  };
};

export default HomePage;
